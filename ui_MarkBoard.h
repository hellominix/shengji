/********************************************************************************
** Form generated from reading UI file 'MarkBoard.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MARKBOARD_H
#define UI_MARKBOARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_MarkBoard
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLabel *myMark;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QLabel *leftMark;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_3;
    QLabel *rightMark;

    void setupUi(QFrame *MarkBoard)
    {
        if (MarkBoard->objectName().isEmpty())
            MarkBoard->setObjectName(QString::fromUtf8("MarkBoard"));
        MarkBoard->resize(222, 98);
        MarkBoard->setStyleSheet(QString::fromUtf8("QFrame { color: white; }"));
        gridLayout = new QGridLayout(MarkBoard);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(MarkBoard);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);

        myMark = new QLabel(MarkBoard);
        myMark->setObjectName(QString::fromUtf8("myMark"));
        myMark->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(myMark, 0, 2, 1, 1);

        label_3 = new QLabel(MarkBoard);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 1, 1, 1);

        leftMark = new QLabel(MarkBoard);
        leftMark->setObjectName(QString::fromUtf8("leftMark"));
        leftMark->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(leftMark, 1, 2, 1, 1);

        label_5 = new QLabel(MarkBoard);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(15, 19, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 2, 1, 1, 1);

        rightMark = new QLabel(MarkBoard);
        rightMark->setObjectName(QString::fromUtf8("rightMark"));
        rightMark->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(rightMark, 2, 2, 1, 1);


        retranslateUi(MarkBoard);

        QMetaObject::connectSlotsByName(MarkBoard);
    } // setupUi

    void retranslateUi(QFrame *MarkBoard)
    {
        MarkBoard->setWindowTitle(QApplication::translate("MarkBoard", "MarkBoard", 0));
        label->setText(QApplication::translate("MarkBoard", "me", 0));
        myMark->setText(QString());
        label_3->setText(QApplication::translate("MarkBoard", "left robot", 0));
        leftMark->setText(QString());
        label_5->setText(QApplication::translate("MarkBoard", "right robot", 0));
        rightMark->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MarkBoard: public Ui_MarkBoard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MARKBOARD_H
