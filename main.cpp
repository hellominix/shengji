#include "MainFrame.h"
#include <QtWidgets/QApplication>
#include <QPixmap>
#include <QBitmap>
#include <QPainter>
#include <QVector>
#include <QSet>
#include "Strategy.h"
#include <QFont>
#include <QTranslator>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
}


int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageOutput);
	QApplication a(argc, argv);

	QTranslator translator;
	translator.load("zh.qm", QApplication::applicationDirPath());
	a.installTranslator(&translator);

	MainFrame w;
	w.setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);
	w.show();

	return a.exec();
}
